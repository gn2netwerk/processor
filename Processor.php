<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph Stäblein
 */

class Processor
{
    public static $extensions       = array('css', 'less', 'js');
    public static $minify_js        = true;
    public static $minify_css       = true;
    public static $compile_less     = true;
    public static $debug            = false; # switch err display & force regenerate


    static public function parse($sourcepath, $eeCachingActive = false, $dev = false)
    {
        global $REX;

        if (!file_exists($sourcepath)) {
            return $sourcepath.'.processor-404-error';
        }

        // if dev context: don't minify, use debug ..
        if ($dev == true) {
            self::$minify_js  = false;
            self::$minify_css = false;
            self::$compile_less = false;
            self::$debug      = true;
        }

        // handle absolute URLs
        $url = parse_url($sourcepath);
        if (isset($url['scheme'])) {
            return $sourcepath;
        }

        // Get file extension
        $ext = pathinfo($sourcepath, PATHINFO_EXTENSION);

        // Skip if wrong filetype
        if(!in_array($ext, self::$extensions)) {
            return $sourcepath;
        }

        // early out?
        if (($ext == "css" && ! self::$minify_css) || ($ext == "js" && ! self::$minify_js)) {
            return $sourcepath;
        }

        // Sanitize
        $sourcepath = urldecode($sourcepath);
        $sourcepath = str_replace(array("\\", "..", chr(0)), '', $sourcepath);
        $sourcepath = ltrim($sourcepath, '/');

        // Cachefile
        $sha1      = substr(sha1_file($sourcepath), 0, 8);
        $out_ext   = $ext == 'less' ? 'css' : $ext;
        $env_ext   = $dev == false ? '' : '.dev';
        $cachepath = str_replace('.'.$ext, '.p.'.$sha1.$env_ext.'.'.$out_ext, $sourcepath);
        // smx_sm: we need a fix for folders containing defined extensions, e.g. ".../typeahead.js/typeahead.css"!
        // only replace in the filename, not in the path!
        $last_slash_pos = strrpos($sourcepath, "/");
        if($last_slash_pos > -1) {
            $sourcepath_path = substr($sourcepath, 0, $last_slash_pos);
            $sourcepath_file = substr($sourcepath, $last_slash_pos);
            $cachepath = $sourcepath_path . str_replace('.'.$ext, '.p.'.$sha1.$env_ext.'.'.$out_ext, $sourcepath_file);
        }
        // special case, keep base filename if only LESS compilation
        /*
        if ($ext == "less" && !self::$minify_css) {
            $cachepath = str_replace('.'.$ext, '.'.$out_ext, $sourcepath);
        }
        */

        // if processed file exists -> nothing to do..
        if(file_exists($cachepath)) {
            return $cachepath;
        }

        // Serve or Generate
        try {
            switch ($ext) {
                case 'less':
                    if(self::$compile_less) {
                        if (!file_exists($cachepath) || self::$debug) {
                            self::clearCache(str_replace($sha1, '*', $cachepath), $eeCachingActive);

                            $less = new lessc;
                            $less->setPreserveComments(false);
                            $less->checkedCompile($sourcepath, $cachepath);
                            if (self::$minify_css) {
                                $CssMin = new CssMin;
                                file_put_contents($cachepath, $CssMin->run(file_get_contents($cachepath)));
                            }
                        }
                    }
                    return $cachepath;
                    break;

                case 'css':
                    if (self::$minify_css) {
                        if((!file_exists($cachepath) || self::$debug)) {
                            self::clearCache(str_replace($sha1, '*', $cachepath), $eeCachingActive);
                            $CssMin = new CssMin;
                            file_put_contents($cachepath, $CssMin->run(file_get_contents($sourcepath)));
                        }
                        return $cachepath;
                    } else {
                        return $sourcepath;
                    }
                    break;

                case 'js':
                    if (self::$minify_js) {
                        if((!file_exists($cachepath) || self::$debug)) {
                            self::clearCache(str_replace($sha1, '*', $cachepath), $eeCachingActive);
                            if (strpos($sourcepath, '.min.js') === false) {
                                file_put_contents($cachepath, trim(JsMin::minify(file_get_contents($sourcepath))));
                            } else {
                                $content = file_get_contents($sourcepath);
                                $content = trim(preg_replace("/\/\*.*?\*\//s", '', $content));
                                file_put_contents($cachepath, $content);
                            }
                        }
                        return $cachepath;
                    } else {
                        return $sourcepath;
                    }
                    break;
            }
        } catch (Exception $e) {
            file_put_contents($cachepath.'.processor-generate-error', var_export($e,true));
            return $cachepath.'.processor-generate-error';
        }

    }

    /**
     * Clear cache for specific file pattern
     *
     * @param            $pattern
     * @param bool|false $eeCachingActive
     */
    static public function clearCache($pattern, $eeCachingActive = false)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = oxConfig::getInstance();
        }
        if($eeCachingActive) {
            // smx_sm: for EE, we only clear the cache when the dyn cache is flushed in the
            // admin area - otherwise we end up with invalid references in cached content files
            // because the old filenames (SHA1!) are still used in the cached HTML files, but 
            // the cached resources have a new filename!
            return;
        }
        foreach ( (array) glob($pattern) as $cacheFile) {
            //echo "<br>deleting file .... " . $cacheFile;
            unlink($cacheFile);
            // also delete merged CSS / JS files
            $ext = pathinfo($cacheFile, PATHINFO_EXTENSION);
            $tmpdir = $oConfig->getConfigParam('gn2_tmprespath');
            $tmpdirPattern = $oConfig->getConfigParam('sShopDir') . $tmpdir . "*." . $ext;
            foreach ( (array) glob($tmpdirPattern) as $mergedCacheFile) {
                unlink($mergedCacheFile);
            }
        }
    }

    /**
     * Completely remove all cache files
     */
    public static function pruneCacheFiles() {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = oxConfig::getInstance();
        }
        $outDir = $oConfig->getOutDir();
        $allJS = self::recursiveCacheFileGlob($outDir, 'js');
        $allCss = self::recursiveCacheFileGlob($outDir, 'css');
        $aCacheFiles = array_merge($allJS, $allCss);
        foreach ( $aCacheFiles as $cacheFile) {
            unlink($cacheFile);
        }
        // and also delete the merged JS and CSS files
        $tmpdir = $oConfig->getConfigParam('sShopDir') . $oConfig->getConfigParam('gn2_tmprespath');
        self::emptyDir($tmpdir);
    }

    /**
     * Find cache files recursively by file extension
     * @param string $dir The base path
     * @param string $ext The file extension without dot, e.g. 'js'
     * @return array
     */
    public static function recursiveCacheFileGlob($dir, $ext) {

        $oDirectory = new RecursiveDirectoryIterator($dir);
        $oIterator = new RecursiveIteratorIterator($oDirectory);
        $aRegex = new RegexIterator($oIterator, "/^.+\.p\..+\.{$ext}$/i", RecursiveRegexIterator::GET_MATCH);

        return array_keys(iterator_to_array($aRegex));
    }

    /**
     * Delete directory contents
     * @param string $path
     */
    public static function emptyDir($path) {
        if($path == "" || !file_exists($path)) {
            return;
        }
        $d = opendir($path);
        while (($filename = readdir($d) ) !== false) {
            $filepath = $path . $filename;
            if (is_file($filepath)) {
                //echo "<br>Deleting $filepath";
                unlink($filepath);
            }
        }
    }

    public static function minifyJS($bool) {
        self::$minify_js = $bool;
    }


    public static function minifyCSS($bool) {
        self::$minify_css = $bool;
    }
    
    public static function compileLess($bool) {
        self::$compile_less = $bool;
    }

    public static function debug($bool) {
        self::$debug = $bool;
    }


}
