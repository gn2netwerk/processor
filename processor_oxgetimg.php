<?php

$url = $_SERVER['REDIRECT_URL'];

$sourcePath = dirname(dirname(dirname(__FILE__))).$url;

if (file_exists($sourcePath)) {

    $sha1Long = sha1_file($sourcePath);
    $sha1 = substr($sha1Long, 0, 8);

    $pathWay = explode(".", $sourcePath);
    $idx = count($pathWay)-1;
    $ext = $pathWay[$idx];
    $pathWay[$idx] =  'p.'.$sha1.'.'.$pathWay[$idx];
    $destPath = implode('.', $pathWay);

    switch($ext) {
        case "png":
            if (!file_exists($destPath)) {
                switch (PHP_OS) {
                    case "Darwin": $cmd = "bin/pngout-osx -y -s0 -f0 ";   break;
                    case "Linux";  $cmd = "bin/pngout-linux -y -s0 -f0 "; break;
                    default: $destPath = $sourcePath; /* restore existing oxid functionality */ break;
                }
                if (isset($cmd)) {
                    $cmd .= escapeshellarg($sourcePath).' '.escapeshellarg($destPath);
                    ob_start();
                    system($cmd);
                    ob_get_clean();
                }
            }
            header('Content-Type:image/png');
        break;
        case "jpeg":
        case "jpg":
            if (!file_exists($destPath)) {
                switch (PHP_OS) {
                    case "Darwin": $cmd = "bin/jpegoptim-osx -f -o ";   break;
                    case "Linux";  $cmd = "bin/jpegoptim-linux -f -o "; break;
                    default: $destPath = $sourcePath; /* restore existing oxid functionality */ break;
                }
                if (isset($cmd)) {
                    $cmd .= escapeshellarg($sourcePath).' --dest=.;';
                    $cmd .= "mv ".escapeshellarg(basename($sourcePath)).' '.escapeshellarg($destPath);
                    ob_start();
                    system($cmd);
                    ob_get_clean();
                }
            }
            header('Content-Type:image/jpeg');
        break;
        case "gif":
            header('Content-Type:image/gif');
            $destPath = $sourcePath;
        break;
    }
    if (file_exists($destPath)) {
        $fp = fopen($destPath, 'rb');
        header("Accept-Ranges: bytes");
        header("Content-Length: ".filesize($destPath));
        header("Last-Modified: ". gmdate('D, d M Y H:i:s T', filemtime($destPath)));
        header("Cache-control: public, max-age=604800");
        header("Expires:".gmdate('D, d M Y H:i:s T', time() + 604800));
        header("Content-Disposition: inline; filename=\"".basename($sourcePath)."\";");
        fpassthru($fp);
    }
}
?>