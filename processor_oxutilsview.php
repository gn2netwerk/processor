<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.1
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 */

class Processor_OxUtilsView extends Processor_OxUtilsView_parent
{
    public function getSmarty($blReload = false)
    {
        if (class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        } else {
            $oConfig = $this->getConfig();
        }
        // only minify in frontend
        if (isAdmin()) {
            return parent::getSmarty($blReload);
        }

        $p = dirname(__FILE__);
        if (!class_exists('Processor')) {
            include_once $p . '/Processor.php';
        }
        if (!class_exists('lessc') && $oConfig->getConfigParam('gn2_compileless')) {
            include_once $p.'/lessc.php';
        }
        if (!class_exists('cssmin') && $oConfig->getConfigParam('gn2_minifycss')) {
            include_once $p . '/cssmin.php';
        }
        if (!class_exists('jsmin') && $oConfig->getConfigParam('gn2_minifyjs')) {
            include_once $p.'/jsmin.php';
        }

        $path = rtrim($oConfig->getConfigParam('sShopDir'), '/');
        $smarty = parent::getSmarty($blReload);
        $smarty->unregister_function('oxstyle');
        $smarty->register_function('oxstyle', array($this,'processor_smarty_function_oxstyle'));
        $smarty->unregister_function('oxscript');
        $smarty->register_function('oxscript', array($this,'processor_smarty_function_oxscript'));

        foreach (array('smarty_function_oxstyle', 'smarty_function_oxscript') as $func) {
            if (!function_exists($func)) {
                $file = $path.'/core/smarty/plugins/'.str_replace('smarty_function_', 'function.', $func).'.php';
                include_once $file;
            }
        }
        return $smarty;
    }

    function processor_smarty_function_oxstyle($params, &$smarty)
    {
        $output = smarty_function_oxstyle($params, $smarty);
        return $this->processor_parse($output);
    }

    function processor_smarty_function_oxscript($params, &$smarty)
    {
        $output = smarty_function_oxscript($params, $smarty);
        return $this->processor_parse($output);
    }

    function processor_parse($output)
    {
        if (strlen($output) > 0) {
            if (class_exists('oxRegistry')) {
                $oConfig = oxRegistry::getConfig();
            } else {
                $oConfig = $this->getConfig();
            }
            
            $noSSL = str_replace('/', '\/', $oConfig->getShopUrl());
            $SSL = str_replace('/', '\/', $oConfig->getSslShopUrl());
            // smx_sm: bugfix - make "?334343" timestamp optional, otherwise module resources (JS, CSS,...) are not processed since those
            // do not get time stamps from OXID in their filenames....
            //$pattern = "/(?P<attr>href|src)=\"(?P<domain>".$noSSL."|".$SSL.")(?P<path>.*?)?(\?(.*))\"/";
            $pattern = "/(?P<attr>href|src)=\"(?P<domain>".$noSSL."|".$SSL.")(?P<path>.*?)?(\?(.*))?\"/";
            $output = preg_replace_callback($pattern, array($this, 'processor_replace_callback'), $output);
        }
        return $output;
    }

    function processor_replace_callback($m)
    {
        if (class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        } else {
            $oConfig = $this->getConfig();
        }
        // smx_sm: check for EE caching
        $blIsEE = $oConfig->getEdition() == "EE";
        $eeCachingActive = $blIsEE && $oConfig->getConfigParam('blUseContentCaching');
        $path = $m['path'];
        if (strpos($m['path'], '.min.') === false && strpos($m['path'], 'out/admin/') === false) {
            $compileLess = $oConfig->getConfigParam('gn2_compileless');
            $minifyCss   = $oConfig->getConfigParam('gn2_minifycss');
            $minifyJs    = $oConfig->getConfigParam('gn2_minifyjs');

            // if debug mode and deactivated minifying
            if (!$oConfig->isProductiveMode() && !$oConfig->getConfigParam('gn2_minify_debug')) {
                $minifyCss   = false;
                $minifyJs    = false;
            }

            // do the work :)
            Processor::compileLess($compileLess);
            Processor::minifyJS($minifyJs);
            Processor::minifyCSS($minifyCss);

            $path = Processor::parse($m['path'], $eeCachingActive);
        }
        $url = $m['attr'].'="'.$m['domain'].$path.'"';
        return $url;
    }
    /**
     * Set smarty include paths
     * @param Smarty $oSmarty Smarty object
     * @return null
     */
    protected function _fillCommonSmartyProperties($oSmarty)
    {
        parent::_fillCommonSmartyProperties($oSmarty);
        if (class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        } else {
            $oConfig = $this->getConfig();
        }
        // not for admin area or dev shops ...
        if (isAdmin() /*|| !$oConfig->isProductiveMode()*/) {
            return;
        }
        // add our smarty directory to global plugin path array...
        $aPluginsDir = $oSmarty->plugins_dir;
        $aPluginsDir[] = $oConfig->getModulesDir()."gn2netwerk/processor/libs/smarty/";
        $oSmarty->plugins_dir = $aPluginsDir;
        if ($oConfig->getConfigParam('gn2_combinejs')) {
            $oSmarty->load_filter('output', 'join_javascript');
        }
        if ($oConfig->getConfigParam('gn2_combinecss')) {
            $oSmarty->load_filter('output', 'join_css');
        }
    }
}
