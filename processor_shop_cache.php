<?php

/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.1
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph St�blein
 * 
 * This module deletes processor JS and CSS cache files like "myscript.p.3434343.js"
 * when the content cache is flushed.
 * In EE with active caching it is necessary that the cached files are NOT deleted as 
 * long as the EE dyn cache files still exists, otherwise those have broken references to the "old"
 * processor cache files!
 */
class processor_shop_cache extends processor_shop_cache_parent {

    /**
     * Flush content cache.
     *
     * @return null
     */
    public function flushContentCache()
    {
        parent::flushContentCache();
        
        // now delete all processor tmp files
        Processor::pruneCacheFiles();
    }
}

?>
