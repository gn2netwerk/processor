<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.1
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph St�blein
 */

/**
 * De-/Activation module for processor
 * Clears tmp contents every time the module is de-/activated
 * to make sure the users don't get a white page when browsing the shop.
 */
class processor_setup extends oxSuperCfg {

    /**
     * Setup routine
     */
    public static function onActivate() {
        self::clearTmp();
    }
    /**
     * Removal routine
     */
    public static function onDeactivate() {
        self::clearTmp();
    }
    
    /**
     * Empty tmp dir
     */
    public static function clearTmp() {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = oxConfig::getInstance();
        }
        $sTmpDir = realpath($oConfig->getShopConfVar('sCompileDir'));
        $aFiles = glob($sTmpDir.'/*{.php,.txt}',GLOB_BRACE);
        $aFiles = array_merge($aFiles, glob($sTmpDir.'/smarty/*.php'));
        $aFiles = array_merge($aFiles, glob($sTmpDir.'/ocb_cache/*.json'));
        if(count($aFiles) > 0)
        {
            foreach($aFiles as $file) {
                @unlink($file);
            }
        }
        Processor::pruneCacheFiles();
    }
}

?>
