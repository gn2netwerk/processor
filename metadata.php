<?php
$sMetadataVersion = '1.1';

$aModule = array(
    'id'           => 'processor',
    'title'        => 'gn2 :: Processor',
    'description'  => 'Processor minifies, compresses and hashes any HTML files,
                       .css and .js files within your OXID-Installation. All
                       resources use \'forever-caching\' or \'fingerprinting\'
                       to prevent unnecessary 304 requests (see
                       <a style="text-decoration:underline;" href="https://developers.google.com/speed/docs/best-practices/caching?hl=de#gc-content">
                       https://developers.google.com/speed/docs/best-practices/caching?hl=de#gc-content</a>).
                       This speeds up your pages quite dramatically.',
    'thumbnail'    => 'processor.png',
    'version'      => '1.5.1',
    'author'       => 'gn2 netwerk',
    'url'          => 'http://www.gn2-netwerk.de',
    'email'        => 'kontakt@gn2-netwerk.de',
    'files'       => array(
        'processor'         => 'gn2netwerk/processor/Processor.php',
        'processor_setup'   => 'gn2netwerk/processor/processor_setup.php',
    ),
    'extend'       => array(
        'oxutilsview'   => 'gn2netwerk/processor/processor_oxutilsview',
        'oxshopcontrol' => 'gn2netwerk/processor/processor_oxshopcontrol',
        'shop_main'     => 'gn2netwerk/processor/processor_shop_main',
        'shop_cache'    => 'gn2netwerk/processor/processor_shop_cache',
    ),
    'events' => array(
        'onActivate' => 'processor_setup::onActivate',
        'onDeactivate' => 'processor_setup::onDeactivate'
    ),
    'settings' => array(
        array('group' => 'GN2PRO_COMPILE',  'name' => 'gn2_compileless',        'type' => 'bool',   'value' => '1'),

        array('group' => 'GN2PRO_MINIFY',   'name' => 'gn2_minifyjs',           'type' => 'bool',   'value' => '1'),
        array('group' => 'GN2PRO_MINIFY',   'name' => 'gn2_minifycss',          'type' => 'bool',   'value' => '1'),
        array('group' => 'GN2PRO_MINIFY',   'name' => 'gn2_minifyhtml',         'type' => 'bool',   'value' => '1'),
        array('group' => 'GN2PRO_MINIFY',   'name' => 'gn2_minify_debug',       'type' => 'bool',   'value' => '0'),

        array('group' => 'GN2PRO_INCLUDE',  'name' => 'gn2_includesmallscripts',    'type' => 'bool',   'value' => '0'),
        array('group' => 'GN2PRO_INCLUDE',  'name' => 'gn2_includesmallcss',        'type' => 'bool',   'value' => '0'),
        array('group' => 'GN2PRO_INCLUDE',  'name' => 'gn2_parseimagesizes', 'type' => 'bool',  'value' => '0'),
        array('group' => 'GN2PRO_INCLUDE',  'name' => 'gn2_include_debug',          'type' => 'bool',   'value' => '0'),

        array('group' => 'GN2PRO_COMBINE',  'name' => 'gn2_combinecss', 'type' => 'bool',  'value' => '0'),
        array('group' => 'GN2PRO_COMBINE',  'name' => 'gn2_combinejs', 'type' => 'bool',  'value' => '0'),
        array('group' => 'GN2PRO_COMBINE',  'name' => 'gn2_tmprespath', 'type' => 'str',  'value' => '/out/tmp/'),
        array('group' => 'GN2PRO_COMBINE',  'name' => 'gn2_excludejs', 'type' => 'arr',  'value' => ''),
        array('group' => 'GN2PRO_COMBINE',  'name' => 'gn2_excludecss', 'type' => 'arr',  'value' => ''),

        array('group' => 'GN2PRO_MISC',     'name' => 'gn2_parseimagesizes',    'type' => 'bool',   'value' => '0'),
        array('group' => 'GN2PRO_MISC',     'name' => 'gn2_emptyclassattr',     'type' => 'bool',   'value' => '0'),
    ),
);
