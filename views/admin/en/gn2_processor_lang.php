<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph Stäblein
 */
$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                                   => 'UTF-8',

    'SHOP_MODULE_GROUP_GN2PRO_MINIFY'           => "Minify",
    'SHOP_MODULE_GROUP_GN2PRO_COMPILE'          => "Compile",
    'SHOP_MODULE_GROUP_GN2PRO_INCLUDE'          => "Include",
    'SHOP_MODULE_GROUP_GN2PRO_MISC'             => "Miscellaneous",
    'SHOP_MODULE_GROUP_GN2PRO_COMBINE'          => "Combine",

    'SHOP_MODULE_gn2_compileless'               => "Compile LESS",

    'SHOP_MODULE_gn2_minifyjs'                  => "Minify Javascript",
    'SHOP_MODULE_gn2_minifycss'                 => "Minify CSS",
    'SHOP_MODULE_gn2_minifyhtml'                => "Minify HTML",
    'SHOP_MODULE_gn2_minify_debug'              => "Also minify in developer mode",

    'SHOP_MODULE_gn2_includesmallscripts'       => "Include small JS files as &lt;script&gt;…&lt;/script&gt; tags (< 2Kb)",
    'SHOP_MODULE_gn2_includesmallcss'           => "Include small CSS files as &lt;style&gt;…&lt;/style&gt; tags (< 2Kb)",
    'SHOP_MODULE_gn2_include_debug'             => "Also include in developer mode",

    'SHOP_MODULE_gn2_parseimagesizes'           => "Add missing width=\"\" and height=\"\" to &lt;img&gt; Tags",
    'SHOP_MODULE_gn2_emptyclassattr'            => "Remove empty class=\"\" attributes",

    'SHOP_MODULE_gn2_combinecss'               => "Combine CSS into one file",
    'SHOP_MODULE_gn2_combinejs'                => "Combine Javascript in one file",
    'SHOP_MODULE_gn2_tmprespath'               => "Path for temporary JS / CSS files (default: '/out/tmp/', please create the directory!)",
    'SHOP_MODULE_gn2_excludecss'               => "Do NOT combine these CSS files (a part of the filename/path is enough, e.g. 'mystyles')",
    'SHOP_MODULE_gn2_excludejs'                => "Do NOT combine these JS files (a part of the filename/path is enough, e.g. 'jquery')",

);
