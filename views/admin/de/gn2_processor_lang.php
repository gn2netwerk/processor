<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph St�blein
 */
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                                   => 'UTF-8',

    'SHOP_MODULE_GROUP_GN2PRO_MINIFY'           => "Minimieren",
    'SHOP_MODULE_GROUP_GN2PRO_COMPILE'          => "Kompilieren",
    'SHOP_MODULE_GROUP_GN2PRO_INCLUDE'          => "Inkludieren",
    'SHOP_MODULE_GROUP_GN2PRO_MISC'             => "Sonstiges",
    'SHOP_MODULE_GROUP_GN2PRO_COMBINE'          => "Zusammenfassen",

    'SHOP_MODULE_gn2_compileless'               => "LESS kompilieren",

    'SHOP_MODULE_gn2_minifyjs'                  => "Javascript minimieren",
    'SHOP_MODULE_gn2_minifycss'                 => "CSS minimieren",
    'SHOP_MODULE_gn2_minifyhtml'                => "HTML minimieren",
    'SHOP_MODULE_gn2_minify_debug'              => "Auch im Entwicklermodus minimieren",

    'SHOP_MODULE_gn2_includesmallscripts'       => "Kleine JS-Dateien inkludieren (< 2Kb)",
    'SHOP_MODULE_gn2_includesmallcss'           => "Kleine CSS-Dateien inkludieren (< 2Kb)",
    'SHOP_MODULE_gn2_include_debug'             => "Auch im Entwicklermodus includieren",

    'SHOP_MODULE_gn2_parseimagesizes'           => "&lt;img&gt; Tags um width=\"\" und height=\"\" erweitern",
    'SHOP_MODULE_gn2_emptyclassattr'            => "Leere class=\"\" Attribute entfernen",

    'SHOP_MODULE_gn2_combinecss'               => "CSS in eine Datei zusammenfassen",
    'SHOP_MODULE_gn2_combinejs'                => "Javascript in eine Datei zusammenfassen",
    'SHOP_MODULE_gn2_tmprespath'               => "Pfad f&uuml;r temp. JS / CSS Dateien (Standard: '/out/tmp/', Verzeichnis bitte anlegen!)",
    'SHOP_MODULE_gn2_excludecss'               => "Diese CSS Dateien NICHT zusammenfassen (Teile des Namens reichen, z.B. 'mystyles')",
    'SHOP_MODULE_gn2_excludejs'                => "Diese JS Dateien NICHT zusammenfassen (Teile des Namens reichen, z.B. 'jquery')",

);
