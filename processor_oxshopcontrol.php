<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.1
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph St�blein
 */

class Processor_OxShopControl extends Processor_OxShopControl_parent
{
    /**
     * render oxView object
     *
     * @param oxView $oViewObject view object to render
     *
     * @return string
     */
    protected function _render($oViewObject)
    {
        $sOutput = parent::_render($oViewObject);
        // only minify in frontend
        if (isAdmin()) {
            return $sOutput;
        }

        if (class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        } else {
            $oConfig = $this->getConfig();
        }

        // check sizes of any scripts & include inline if < 1kb
        if ($oConfig->getConfigParam('gn2_includesmallscripts')) {
            if ($oConfig->isProductiveMode() || $oConfig->getConfigParam('gn2_include_debug')) {
                $sOutput = $this->_includeSmallScripts($sOutput);
            }
        }

        if ($oConfig->getConfigParam('gn2_includesmallcss')) {
            if ($oConfig->isProductiveMode() || $oConfig->getConfigParam('gn2_include_debug')) {
                $sOutput = $this->_includeSmallCSS($sOutput);
            }
        }

        // fix any IMG Tags that don't have widths and heights
        if ($oConfig->getConfigParam('gn2_parseimagesizes')) {
            $sOutput = $this->_parseImageSizes($sOutput);
        }


        // Fix empty class attributes
        if ($oConfig->getConfigParam('gn2_emptyclassattr')) {
            $sOutput = preg_replace("/class=\"\"/", "", $sOutput);
        }

        if ($oConfig->getConfigParam('gn2_minifyhtml')) {
            if ($oConfig->isProductiveMode() || $oConfig->getConfigParam('gn2_minify_debug')) {
                $sOutput = $this->_trim_whitespace($sOutput);
            }
        }

        return $sOutput;
    }
    
    /**
     * Function to trim any whitespace with some exceptions, e.g. inside script tags
     * Removes multi spaces, tabs, newlines, ...
     * @see http://stackoverflow.com/questions/5312349/minifying-final-html-output-using-regular-expressions-with-codeigniter
     * @param string $text The text to trim
     * @return string The trimmed text
     */
    protected function _trim_whitespace($html) //
    {
        $pattern = '%# Collapse whitespace everywhere but in blacklisted elements.
            (?>             # Match all whitespans other than single space.
              [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
            | \s{2,}        # or two or more consecutive-any-whitespace.
            |<!--(?!\[if).*?-->+ # or comments which are not IE conditional comments (smx_sm)
            ) # Note: The remaining regex consumes no text at all...
            (?=             # Ensure we are not in a blacklist tag.
              [^<]*+        # Either zero or more non-"<" {normal*}
              (?:           # Begin {(special normal*)*} construct
                <           # or a < starting a non-blacklist tag.
                (?!/?(?:textarea|pre|script)\b)
                [^<]*+      # more non-"<" {normal*}
              )*+           # Finish "unrolling-the-loop"
              (?:           # Begin alternation group.
                <           # Either a blacklist start tag.
                (?>textarea|pre|script)\b
              | \z          # or end of file.
              )             # End alternation group.
            )  # If we made it here, we are not in a blacklist tag.
            %Six';
        $html = preg_replace($pattern, " ", $html);

        return $html;
    }

    protected function _parseImageSizes($html)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = $this->getConfig();
        }
        $noSSL = str_replace('/', '\/', $oConfig->getShopUrl());
        $SSL = str_replace('/', '\/', $oConfig->getSslShopUrl());
        $pattern = "/(?P<start><img.*?src=\")(?P<domain>".$noSSL."|".$SSL.")(?P<src>.*?)(?P<end>\".*?>)/";
        $html = preg_replace_callback($pattern, array($this, '_parseImageSizesCallback'), $html);
        return $html;
    }

    protected function _parseImageSizesCallback($m)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = $this->getConfig();
        }
        $imgTag = $m['start'].$m['domain'].$m['src'];

        $file = $oConfig->getConfigParam('sShopDir').'/'.$m['src'];
        if (file_exists($file) && strpos($m[0], 'width="') === false && strpos($m[0], 'height="') === false) {
            $size = getimagesize($file);
            if (is_array($size)) {
                $m['end'] = substr($m['end'], 0, -1).' '.$size[3].'>';
            }
        }
        $imgTag .= $m['end'];
        return $imgTag;
    }

    protected function _includeSmallScripts($html)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = $this->getConfig();
        }
        $noSSL = str_replace('/', '\/', $oConfig->getShopUrl());
        $SSL = str_replace('/', '\/', $oConfig->getSslShopUrl());
        $pattern = "/<script.*? src=\"(?P<domain>".$noSSL."|".$SSL.")(?P<src>.*?)\">.*?<\/script>/";

        $html = preg_replace_callback($pattern, array($this, '_includeSmallScriptsCallback'), $html);
        return $html;
    }

    protected function _includeSmallScriptsCallback($m)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = $this->getConfig();
        }
        $file = $oConfig->getConfigParam('sShopDir').'/'.$m['src'];
        if (file_exists($file)) {
            if (filesize($file) < 1024) {
                return "<script>".trim(file_get_contents($file))."\n</script>";
            }
        }
        return $m[0];
    }


    protected function _includeSmallCSS($html)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = $this->getConfig();
        }
        $noSSL = str_replace('/', '\/', $oConfig->getShopUrl());
        $SSL = str_replace('/', '\/', $oConfig->getSslShopUrl());
        $pattern = "/<link.*?text\/css.*? href=\"(?P<domain>".$noSSL."|".$SSL.")(?P<src>.*?)\".*?>/";

        $html = preg_replace_callback($pattern, array($this, '_includeSmallCSSCallback'), $html);
        return $html;
    }

    protected function _includeSmallCSSCallback($m)
    {
        if(class_exists('oxRegistry')) {
            $oConfig = oxRegistry::getConfig();
        }
        else {
            $oConfig = $this->getConfig();
        }
        $file = $oConfig->getConfigParam('sShopDir').'/'.$m['src'];
        if (file_exists($file)) {
            $size = filesize($file);
            if ($size === 0) {
                return '';
            } else if (filesize($file) < 1000) {
                $content = trim(file_get_contents($file));
                /* Don't include if the file contains any image paths */
                if (strpos($content, 'url(') !== false) {
                    return $m[0];
                }
                return "<style type=\"text/css\">".$content."\n</style>";
            }
        }
        return $m[0];
    }
}
