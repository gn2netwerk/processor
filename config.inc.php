<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph St�blein
 */

require_once 'Processor.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/lessc.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/cssmin.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/jsmin.php';
