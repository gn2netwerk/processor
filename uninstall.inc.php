<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.5.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author Stefan Moises <moises@proudsourcing.de>
 * @author jdlx <honeypot@rexdev.de>
 * @author Christoph St�blein
 */

$REX['ADDON']['install']['processor'] = 0;
