<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty join_css outputfilter plugin
 *
 * File:     outputfilter.join_css.php<br>
 * Type:     outputfilter<br>
 * Name:     join_css<br>
 * Date:     Jan 03, 2008<br>
 * Purpose:  join togther css into a single file
 * Install:  Drop into the plugin directory, call
 *           <code>$smarty->load_filter('output','join_css');</code>
 *           from application. You should specify your cachedir below.
 * @author   Leon Chevalier <http://aciddrop.com>
 * @author   Stefan Moises <moises@shoptimaxx.de>
 * @version  1.1
 * @param string
 * @param Smarty
 */
function smarty_outputfilter_join_css($source, &$smarty)
{
    if(class_exists('oxRegistry')) {
        $oConfig = oxRegistry::getConfig();
    }
    else {
        $oConfig = oxConfig::getInstance();
    }
    return _joiner(array('cachedir' => $oConfig->getConfigParam('gn2_tmprespath'),
        'tag' => 'link',
        'type' => 'text/css',
        'ext' => 'css',
        'src' => 'href',
        'rel' => 'stylesheet',
        'self_close' => true), $source);
}

function _joiner($options, $source)
{
    if(trim($source) == '') {
        return $source;
    }
    /* @var $oConfig oxConfig */
    if(class_exists('oxRegistry')) {
        $oConfig = oxRegistry::getConfig();
    }
    else {
        $oConfig = oxConfig::getInstance();
    }
    $sShopUrl = $oConfig->getShopUrl();
    if($oConfig->isSsl()) {
        $sShopUrl = $oConfig->getSslShopUrl();
    }

    $cachedir = $oConfig->getConfigParam('sShopDir').$options['cachedir'];
    // should we cache the full CSS file?
    $blUseCachedFile = true;

    // use DOM to get all CSS resources
    $script_array = array();
    $doc = new DOMDocument();
    // ignore any parser warnings...
    libxml_use_internal_errors(true);
    $doc->loadHTML($source);
    libxml_clear_errors();
    if($doc) {
        foreach($doc->getElementsByTagName($options['tag']) as $n) {
            $plainCss = $doc->saveHTML($n);
            if (stripos($plainCss, $options['src']."=") !== FALSE && stripos($plainCss,
                    $options['rel']) !== FALSE && !in_array($plainCss,
                    $script_array)) {
                $script_array[] = str_replace('.css"', '.css" /', trim($plainCss));
            }
        }
    }
    //DumpVar($script_array);

    if (is_array($script_array)) {
        //Get the cache hash
        $cache_file = md5(implode("_", $script_array));
        //Remove empty sources and optionally keep specific files...
        $aFilesToKeep = array();
        if(is_array($oConfig->getConfigParam('gn2_excludecss'))) {
            $aFilesToKeep = $oConfig->getConfigParam('gn2_excludecss');
        }
        //Remove empty sources
        foreach ($script_array AS $key => $value) {
            foreach($aFilesToKeep as $sFile) {
                if(stripos($value, $sFile) !== FALSE) {
                    unset($script_array[$key]);
                    continue;
                }
            }
            preg_match("!".$options['src']."=\"(.*?)\"!is", $value, $src);
            // if empty src OR not from shop domain, ignore!
            if (!$src[1] || strpos($src[1], $sShopUrl) === FALSE) {
                unset($script_array[$key]);
            }
        }

        //Check if the cache file exists
        if ($blUseCachedFile && file_exists($cachedir.$cache_file.".$options[ext]")) {
            $source = _remove_scripts($script_array, $source);
            $source = str_replace("@@marker@@",
                "<".$options['tag']." type=\"".$options['type']."\" ".$options['src']."=\"".$sShopUrl.$options['cachedir']."/$cache_file.$options[ext]\" rel=\"stylesheet\" />",
                $source);
            return $source;
        }

        //Create file
        foreach ($script_array AS $key => $value) {
            //Get the src
            preg_match("!".$options['src']."=\"(.*?)\"!is", $value, $src);
            $src[1]      = str_replace($sShopUrl, "", $src[1]);
            $relativePath = str_replace(basename($src[1]), "", $src[1]);
            $current_src = $oConfig->getConfigParam('sShopDir').$src[1];

            //Get the code
            if (file_exists($current_src)) {
                $tmpContent = convert_paths_to_absolute(file_get_contents($current_src),
                    $sShopUrl.$relativePath);
                $contents .= $tmpContent."\n";
                if ($key == count($script_array) - 1) { //Remove script
                    $source = str_replace($value, "@@marker@@", $source);
                } else {
                    $source = str_replace($value, "", $source);
                }
            }
        }

        //Write to cache and display
        if ($contents) {
            if ($fp = fopen( $cachedir . $cache_file.'.'.$options[ext], 'wb')) {
                fwrite($fp, $contents);
                fclose($fp);
                //Create the link to the new file
                $newfile = "<".$options['tag']." type=\"".$options['type']."\" $options[src]=\"".$sShopUrl.$options['cachedir']."/$cache_file.".$options[ext]."\"";
                if ($options['rel']) {
                    $newfile .= " rel=\"".$options['rel']."\"";
                }
                if ($options['self_close']) {
                    $newfile .= " />";
                } else {
                    $newfile .= "></".$options['tag'].">";
                }

                $source = str_replace("@@marker@@", $newfile, $source);
            }
        }
    }

    return $source;
}

/**
 * Remove scripts / marker when using cached file
 * @param array $script_array
 * @param string $source
 * @return string
 */
function _remove_scripts($script_array, $source)
{
    foreach ($script_array as $key => $value) {

        if ($key == count($script_array) - 1) { //Remove script
            $source = str_replace($value, "@@marker@@", $source);
        } else {
            $source = str_replace($value, "", $source);
        }
    }
    return $source;
}

/**
 * Find background images in the CSS and convert their paths to absolute
 * @param string $content
 * @param string $path
 * @return string The replaced CSS content
 */
function convert_paths_to_absolute($content, $path)
{
    preg_match_all("/url\((.*?)\)/is", $content, $matches);
    if (count($matches[1]) > 0) {
        $counter = 0;
        foreach ($matches[1] as $key => $file) {
            if (strstr($file, "data:")) { //Don't touch data URIs
                continue;
            }
            $counter++;
            $original_file = trim($file);
            $file          = preg_replace("@'|\"@", "", $original_file);
            if (substr($file, 0, 1) != "/" && substr($file, 0, 4) != "http") { //Not absolute
                $absolute_path    = $path.$file;
                //echo "<br>File: $file Abs. path: " . $absolute_path;
                $marker           = md5($counter);
                $markers[$marker] = $absolute_path;
                $content          = str_replace($original_file, $marker,
                    $content);
            }
        }
    }

    if (!empty($markers) && is_array($markers)) {
        //Replace the markers for the real path
        foreach ($markers AS $md5 => $real_path) {
            $content = str_replace($md5, $real_path, $content);
        }
    }
    return $content;
}
?>
