<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty join_javascript outputfilter plugin
 *
 * File:     outputfilter.join_javascript.php<br>
 * Type:     outputfilter<br>
 * Name:     join_javascript<br>
 * Date:     Jan 03, 2008<br>
 * Purpose:  join togther javascript into a single file
 * Install:  Drop into the plugin directory, call
 *           <code>$smarty->load_filter('output','join_javascript');</code>
 *           from application. You should specify your cachedir below.
 * @author   Leon Chevalier <http://aciddrop.com>
 * @author   Stefan Moises <moises@shoptimaxx.de>
 * @version  1.1
 * @param string
 * @param Smarty
 */
function smarty_outputfilter_join_javascript($source, &$smarty)
{
    if(class_exists('oxRegistry')) {
        $oConfig = oxRegistry::getConfig();
    }
    else {
        $oConfig = oxConfig::getInstance();
    }

    return _joiner_js(array('cachedir' => $oConfig->getConfigParam('gn2_tmprespath'),
        'tag' => 'script',
        'type' => 'text/javascript',
        'ext' => 'js',
        'src' => 'src',
        'self_close' => false), $source);
}

function _joiner_js($options, $source)
{
    if(trim($source) == '') {
        return $source;
    }
    /* @var $oConfig oxConfig */
    if(class_exists('oxRegistry')) {
        $oConfig = oxRegistry::getConfig();
    }
    else {
        $oConfig = oxConfig::getInstance();
    }
    $sShopUrl = $oConfig->getShopUrl();
    if($oConfig->isSsl()) {
        $sShopUrl = $oConfig->getSslShopUrl();
    }

    $cachedir = $oConfig->getConfigParam('sShopDir').$options['cachedir'];
    // should we cache the full CSS file?
    $blUseCachedFile = true;

    $script_array = array();
    $doc = new DOMDocument();
    // ignore any parser warnings...
    libxml_use_internal_errors(true);
    $doc->loadHTML($source);
    libxml_clear_errors();
    if($doc) {
        foreach($doc->getElementsByTagName($options['tag']) as $n) {
            $plainJs = $doc->saveHTML($n);
            if (stripos($plainJs, $options['src']."=") !== FALSE && !in_array($plainJs,
                    $script_array)) {
                $script_array[] = $plainJs;
            }
        }
    }

    if (is_array($script_array)) {
        //Get the cache hash
        $cache_file = md5(implode("_", $script_array));
        //Remove empty sources and optionally keep specific files...
        $aFilesToKeep = array();
        if(is_array($oConfig->getConfigParam('gn2_excludejs'))) {
            $aFilesToKeep = $oConfig->getConfigParam('gn2_excludejs');
        }
        foreach ($script_array as $key => $value) {
            foreach($aFilesToKeep as $sFile) {
                if(stripos($value, $sFile) !== FALSE) {
                    unset($script_array[$key]);
                    continue;
                }
            }
            preg_match("!".$options['src']."=\"(.*?)\"!is", $value, $src);
            // if empty src OR not from shop domain, ignore!
            if (!$src[1] || strpos($src[1], $sShopUrl) === FALSE) {
                unset($script_array[$key]);
            }
        }
        // reset array keys after removal of elements!
        $script_array = array_values($script_array);

        //Check if the cache file exists
        if ($blUseCachedFile && file_exists($cachedir.$cache_file.".$options[ext]")) {
            $source = _remove_scripts_js($script_array, $source);
            $source = str_replace("@@jsmarker@@",
                "<".$options['tag']." type=\"".$options['type']."\" ".$options['src']."=\"".$sShopUrl.$options['cachedir']."$cache_file.$options[ext]\"></script>",
                $source);
            return $source;
        }

        //Create file
        foreach ($script_array as $key => $value) {
            //Get the src
            preg_match("!".$options['src']."=\"(.*?)\"!is", $value, $src);
            $src[1]      = str_replace($sShopUrl, "", $src[1]);

            $current_src = $oConfig->getConfigParam('sShopDir').$src[1];

            //Get the code
            if (file_exists($current_src)) {
                $contents .= file_get_contents($current_src)."\n";
                // the last included script may be a browser specific one with IE conditionals
                if ($key == 0) { //Remove script
                    $source = str_replace($value, "@@jsmarker@@", $source);
                } else {
                    $source = str_replace($value, "", $source);
                }
            }
        }


        //Write to cache and display
        if ($contents) {
            if ($fp = fopen( $cachedir . $cache_file.'.'.$options[ext], 'wb')) {
                fwrite($fp, $contents);
                fclose($fp);
                //echo "<br>File written... " . $cachedir . $cache_file.'.'.$options[ext];
                //Create the link to the new file
                $newfile = "<".$options['tag']." type=\"".$options['type']."\" $options[src]=\"".$sShopUrl.$options['cachedir']."$cache_file.".$options[ext]."\"";

                if ($options['rel']) {
                    $newfile .= "rel=\"".$options['rel']."\"";
                }

                if ($options['self_close']) {
                    $newfile .= " />";
                } else {
                    $newfile .= "></".$options['tag'].">";
                }

                $source = str_replace("@@jsmarker@@", $newfile, $source);
            }
        }
    }

    return $source;
}

/**
 * Remove scripts / marker when using cached file
 * @param array $script_array
 * @param string $source
 * @return string
 */
function _remove_scripts_js($script_array, $source)
{
    foreach ($script_array as $key => $value) {

        //if ($key == count($script_array) - 1) { //Remove script
        // the last included script may be a browser specific one with IE conditionals
        if ($key == 0) { //Remove script
            $source = str_replace($value, "@@jsmarker@@", $source);
        } else {
            $source = str_replace($value, "", $source);
        }
    }

    return $source;
}
?>
